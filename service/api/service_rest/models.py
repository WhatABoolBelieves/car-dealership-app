from django.db import models
from django.urls import reverse

# Create your models here.

class Appointment(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    name = models.CharField(max_length=200)
    date = models.DateTimeField()
    tech = models.CharField(max_length=200)
    reason = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_detail_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name

    @classmethod
    def create(cls, **kwargs):
        appointment = cls(**kwargs)
        appointment.save()
        return appointment

class Automobile(models.Model):
    vin = models.CharField(max_length=17, unique=True)

class Technician(models.Model):
    name = models.CharField(max_length=200)
    emp_num = models.PositiveSmallIntegerField(unique=True)

    @classmethod
    def create(cls, **kwargs):
        technician = cls(**kwargs)
        technician.save()
        return technician

class History(models.Model):
    vin = models.CharField(max_length=100)
    name = models.CharField(max_length=200)
    date = models.DateTimeField()
    tech = models.CharField(max_length=200)
    reason = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_history_detail_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name

    @classmethod
    def create(cls, **kwargs):
        history = cls(**kwargs)
        history.save()
        return history