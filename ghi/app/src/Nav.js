import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">List of manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new/">Create a manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models">List of models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/new/">Create a new model</NavLink>
            </li>

            {/* <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles">List of cars</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new/">Create a new car</NavLink>
            </li> */}

            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles">List of automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new/">Create a new automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments">List of appointments</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/appointments/new/">Enter a service appointment</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="history/">Service history</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/technicians/new/">Enter a technician</NavLink>
              {/* <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink> */}
              {/* commenting out the above because Home and CarCar landed on same page */}
            </li>
            <li className="nav=item">
              <NavLink className="nav-link" aria-current="page" to="/salespeople/new">
                New sales person
              </NavLink>
            </li>
            <li className="nav=item">
              <NavLink className="nav-link" aria-current="page" to="/customers/new">
                New potential customer
              </NavLink>
            </li>
            <li className="nav=item">
              <NavLink className="nav-link" aria-current="page" to="/salerecords/new">
                New sale record
              </NavLink>
              <NavLink className="nav-link" aria-current="page" to="/salerecords">
                List of sale records
              </NavLink>
              <NavLink className="nav-link" aria-current="page" to="/salesperson/history">
                Sales history
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
