import React from 'react';


class ModelList extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        models: [],
      }
    };

    async getModelData() {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ models: data.models })
        }
    }

    async componentDidMount() {
        this.getModelData()
    }

    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Models</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.models.map(model => {
                        return (
                            <tr key={model.href}>
                                <td>{ model.name }</td>
                                <td>{ model.manufacturer.name }</td>
                                <td>{ model.picture_url }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        )
    }
}

export default ModelList;