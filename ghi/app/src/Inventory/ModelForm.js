import React from 'react';

class ModelForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      picture_url: '',
      manufacturer: '',

    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const newState= {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    const modelUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      const newModel = await response.json();
      console.log(newModel);
      this.setState({
        name: '',
        picture_url: '',
        manufacturer: '',
      });
    }

  }

  render() {
    return(
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new model</h1>
            <form onSubmit={this.handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.name} placeholder="Model name" type="text" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.picture_url} placeholder="picture" type="text" id="picture_url" className="form-control" />
                <label htmlFor="url">URL</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.manufacturer.name} placeholder="manufacturer" type="text" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <button className='btn btn-primary'>Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ModelForm;
