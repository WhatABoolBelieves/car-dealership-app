import React from 'react';

class AppointmentHistory extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        appointments: [],
      }
      this.getHistoryData = this.getHistoryData.bind(this);
      this.vinFilter = this.vinFilter.bind(this);
      this.theVin = this.theVin.bind(this);
    };
    
    async getHistoryData() {
        const url = 'http://localhost:8080/api/history/';
        const response = await fetch(url);
        console.log(response)
        if (response.ok) {
            const data = await response.json();
            this.setState({ appointments: data.appointments });
        }
    }

    async componentDidMount() {
        this.getHistoryData();
    }

    vinFilter() {
        for (let appointment of this.state.appointments) {
            if (appointment['vin'] === this.state.vin) {
                let temp = this.state.appointments;
                temp.push(appointment);
                this.setState({ appointments: temp});
            }
        }
    }

    theVin(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }
    
    render() {
        return (
        <div>
            <form>
                <div className="col-auto">
                    <input onChange={this.theVin} type="search" className="form-control" id="vinsearch" />
                </div>
                <div className="col-auto">
                    <button onClick={this.vinFilter} type="button" className="btn btn-primary mb-2">Search VIN</button>
                </div>
            </form>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date/Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.appointments.map(appointments => {
                        return (
                            <tr>
                                <td>{ appointments.vin }</td>
                                <td>{ appointments.name }</td>
                                <td>{ appointments.date }</td>
                                <td>{ appointments.tech }</td>
                                <td>{ appointments.reason }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        )
    }
}

export default AppointmentHistory;