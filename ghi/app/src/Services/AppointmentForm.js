import React from 'react';

class AppointmentForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      vin: '',
      name: '',
      date: '',
      tech: '',
      reason: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const newState= {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    const appointmentUrl = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      console.log(newAppointment);
      this.setState({
        vin: '',
        name: '',
        date: '',
        tech: '',
        reason: '',
      });
    }
  
  }

  render() {
    return(
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.vin} placeholder="VIN"  required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.name} placeholder="Customer Name"  required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.date} placeholder="Date/Time"  required type="datetime-local" name="date" id="date" className="form-control" />
                <label htmlFor="date">Date/Time</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.tech} placeholder="Technician"  required type="text" name="tech" id="tech" className="form-control" />
                <label htmlFor="tech">Technician</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.reason} placeholder="Reason"  required type="text" name="reason" id="reason" className="form-control" />
                <label htmlFor="reason">Reason</label>
              </div>
              <button class='btn btn-primary'>Create appointment</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AppointmentForm;
