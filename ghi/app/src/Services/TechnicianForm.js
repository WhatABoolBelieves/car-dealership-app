import React from 'react';

class TechnicianForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      emp_num: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const newState= {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    const appointmentUrl = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    console.log("DATA  DATA", data)
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      console.log(newAppointment);
      this.setState({
        name: '',
        emp_num: '',
      });
    }
  
  }

  render() {
    return(
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new technician</h1>
            <form onSubmit={this.handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.name} placeholder="name"  required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Technician Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.emp_num} placeholder="emp_num"  required type="number" min="0" name="emp_num" id="emp_num" className="form-control" />
                <label htmlFor="emp_num">Employee Number</label>
              </div>
              <button class='btn btn-primary'>Create technician</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default TechnicianForm;
