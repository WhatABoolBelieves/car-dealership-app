from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder

from .models import SalesPerson, Customer, SaleRecord, AutomobileVO


class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
    ]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "color",
        "year",
        "model",
        "manufacturer",
        "has_sold",
        "picture_url"
    ]


class SaleRecordListEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "automobile",
        "salesperson",
        # "salesperson.employee_number",
        "customer",
        "price",
    ]

    encoders = {
        "salesperson": SalesPersonListEncoder(),
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = SalesPerson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalesPersonListEncoder,
            )
    else:
        content = json.loads(request.body)

        salesperson = SalesPerson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonListEncoder,
            safe=False,
            )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
            )
    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            CustomerListEncoder,
            safe=False,
            )


@require_http_methods(["GET", "POST"])
def api_list_salerecords(request):
    if request.method == "GET":
        salerecords = SaleRecord.objects.all()
        return JsonResponse(
            {"salerecords": salerecords},
            encoder=SaleRecordListEncoder,
            )
    else:
        content = json.loads(request.body)

        salerecord = SaleRecord.objects.create(**content)
        return JsonResponse(
            salerecord,
            SaleRecordListEncoder,
            safe=False,
            )


# Need instance of customer from database that matches customer names, for
# form fields w dropdown. For how we've done post requests in the past